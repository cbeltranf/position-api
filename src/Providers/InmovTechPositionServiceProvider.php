<?php

namespace Inmovsoftware\PositionApi\Providers;

use Illuminate\Support\ServiceProvider;
class InmovTechPositionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('Inmovsoftware\PositionApi\Models\V1\Position');
        $this->app->make('Inmovsoftware\PositionApi\Http\Controllers\V1\PositionController');

    }


}
